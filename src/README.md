# Introduction

Hi, welcome!

My name is Victor. I'm a software developer who happens to like [Go](https://golang.org) and [Nix(OS)](https://nixos.org).

This website is a place I use to store my notes and thoughts about things I'm interested. Don't be bothered if things feel incomplete or confusing. You're closer to my shower thoughts than a finished book. See [[96hs07jr]].

If you'd like to reach me, try these:

- [Email](mailto:freirel.victor@gmail.com)
- [Telegram](https://t.me/ratsclub)
- [Discord](https://discordapp.com/users/204643170250784768)

---
## Posts

- [[dsxahvyj]]