# Buy and Hold

> He who wishes to be rich in a day will be hanged in a year.
> > **Leonardo da Vinci**

There's a huge difference between a trader and a holder. They are not excluding *philosophies*, you can execute both at the same time (although the trader will definitely lose most of the time). But what's the philosophy behind the holder?

The holder is a business partner looking after the success of the company in the long term, in other words **he doesn't seek price as he's interested in value**. Price and value cannot be mixed together in his decisions as it'll lead to decisions based on emotion. He'll certainly buy when prices are up because it feels like he made the best choice and feel hopeless when prices are down, making him sell. This behavior will endure until all his money is gone.

Basically the holder is after a solid company that has a great administration and delivers consistent profit over the years to become partner.