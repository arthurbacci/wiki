# Nix

> Nix is a package manager and build system that parses reproducible build instructions specified in the Nix Expression Language
> > **NixOS Wiki**

# NixOS

> NixOS a Linux distribution based on the purely functional package management system Nix, that is composed using modules and packages defined in the Nixpkgs project. 
> > **NixOS Manual**

---
**Resources**

- [NixOS Manual](https://nixos.org/manual/)
- [NixOS Unofficial Wiki](https://nixos.wiki)
- [Nixpkgs Search](https://search.nixos.org/)
- [Nixpkgs repository](https://github.com/NixOS/nixpkgs)