# Profit taking makes you poor

> Compound interest is the eighth wonder of the world. He who understands it, earns it; He who doesn't, pays it.
> > **Albert Einstein**

We've been told throughout our entire lives that we should take our profits from stocks, real estate and any other asset growing on **price** (not in value). **This is the first step to blow your patrimony up.**

One thing to keep in mind is that what makes you rich is the accumulation of goods and make usage of the income those goods produce. To make it simple, consider the following example:

An individual possesses enough real estate to generate $30k of income every month. In other words, he could buy a new property every year or so. Would it be a good move to sell some of his property because their price went up? **Of course not**, he would eventually sell it and not be able to buy it back because the prices all went up. Even worse, he'd not get the entire value of the property as he'd lose some of it to taxes and fees, and he'd also lose part of his monthly income.

Never forget the chart below:

![Compound Interest vs Simple Interest chart](assets/images/compound_chart.png)