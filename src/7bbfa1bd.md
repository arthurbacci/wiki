# Production-ready HTTP Server in Go

The ``net`` package has some _gotchas_ (see [[17041abe]]) and one of them is the [ListenAndServe](https://golang.org/src/net/http/server.go?s=97511:97566#L3108) function:

```go
func ListenAndServe(addr string, handler Handler) error {
    server := &Server{Addr: addr, Handler: handler}
    return server.ListenAndServe()
}
```

This is absolutely **not** production-ready as seen in [this](https://web.archive.org/web/20201002213420/https://blog.cloudflare.com/exposing-go-on-the-internet/) Cloudflare article. It doesn't even provide any form of timeouts (see [[5b9be2b2]]).

A proper server configuration would look like the code below:

```go
srv := &http.Server{
    Addr:    addr,
    Handler: handler,
    ReadTimeout:  5 * time.Second,
    WriteTimeout: 10 * time.Second,
    IdleTimeout:  120 * time.Second,
    TLSConfig: &tls.Config{
        NextProtos:       []string{"h2", "http/1.1"},
        MinVersion:       tls.VersionTLS12,
        CurvePreferences: []tls.CurveID{tls.CurveP256, tls.X25519}
        CipherSuites: []uint16{
            tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
            tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
            tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,
            tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,
            tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
            tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
        },
        PreferServerCipherSuites: true,
    }
}
log.Println(srv.ListenAndServe())
```

---
**Resources**

- [Increasing http.Server boilerplate](https://web.archive.org/web/20201002221100/https://bojanz.github.io/increasing-http-server-boilerplate-go/)
- [So you want to expose Go on the Internet](https://web.archive.org/web/20201002213420/https://blog.cloudflare.com/exposing-go-on-the-internet/)
