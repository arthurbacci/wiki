# Websites I visit

## Blogs

- [100 rabbits](https://100r.co)
- [Andyʼs working notes](https://notes.andymatuschak.org/About_these_notes)
- [Astral Codex Ten](https://astralcodexten.substack.com/)
- [Christine Dodrill](https://christine.website/)
- [Drew DeVault](https://drewdevault.com/)
- [Everything I know](https://wiki.nikitavoloboev.xyz/)
- [Gwern Branwen](https://www.gwern.net/index)

## Discussion

- [Lobste.rs](https://lobste.rs)
- [SchemeBBS](https://textboard.org)