# mbsync

[mbsync](https://isync.sourceforge.io/mbsync.html) is a command line application which synchronizes mailboxes; currently Maildir and IMAP4 mailboxes are supported. New messages, message deletions and flag changes can be propagated both ways; the operation set can be selected in a fine-grained manner.

Synchronization is based on unique message identifiers (UIDs), so no identification conflicts can occur (unlike with some other mail synchronizers).

Synchronization state is kept in one local text file per mailbox pair; these files are protected against concurrent mbsync processes. Mailboxes can be safely modified while mbsync operates. Multiple replicas of each mailbox can be maintained.
