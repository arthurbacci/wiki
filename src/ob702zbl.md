# Reading list

- [ ] The Design of Everyday Things - Don Norman
- [ ] Brain Rules for Baby: How to Raise a Smart and Happy Child from Zero to Five - John Medina
- [ ] The Intelligent Investor - Benjamin Graham
- [ ] Predictably Irrational, Revised and Expanded Edition: The Hidden Forces That Shape Our Decisions - Dan Ariely