# Go's paradox

> Go’s paradox is that error handling is core to the language yet the language doesn’t prescribe how to handle errors. Community efforts have been made to improve and standardize error handling but many miss the centrality of errors within our application’s domain. That is, your errors are as important as your Customer and Order types
>
> > **Ben Johnson**

---
**Resources**

[Ben Johnson - Failure is your Domain](https://web.archive.org/save/https://middlemost.com/failure-is-your-domain/)
