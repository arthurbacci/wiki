# Thoughts about NixOS

I've running NixOS on my personal machine for about 2 years or so and I can't see myself running other system on my machine. I installed it thinking it was just another Linux distribution with it's own package manager. At first it was a huge pain to deal with as I didn't research much what it was about. Nothing worked as I expected. I couldn't even download and run a binary! It took me a week to understand how things were expected to work.

Today I don't see NixOS as a Linux Distribution. It's a whole new concept. The idea is that your system configuration is stateless and most of your system is read-only. You'll see a lot of non-sense paths and weird hashes all over the place too.

Below is the configuration needed for a complete GNOME environment.

```nix
services.xserver.enable = true;
services.xserver.displayManager.gdm.enable = true;
services.xserver.desktopManager.gnome3.enable = true;
environment.systemPackages = with pkgs; [ gnome3.gnome-tweaks ];
```

## Pros

- **Configure once, run everywhere**: Yes, just like this. It was mind-blowing to see my NixOS configuration run the same way in different computers.

- **Rollback changes**: Every time you rebuild your system, you'll see a new GRUB entry with your newest *version* and all the old ones. You can just select older versions if anything goes wrong.

- **Number of packages**: According to the Nixpkgs repository[^1] it has [over 60,000 software packages that can be installed with the Nix package manager](https://github.com/NixOS/nixpkgs). You can always open a Pull Request adding or requesting the software you want in case it's not there yet.

## Cons

- **Running dynamically compiled binaries**: This is for me the worst part of NixOS. Dynamic linked libraries will look for a path that won't exist in your system so you have to look for *workarounds*[^2]. Most of the time I just run a Docker container to run binaries off of the internet.

- **Disk space usage**: As you can have multiple versions of the same libraries and multiple derivations of your own system, it's not that unusual to use a lot storage space[^3].

- **Lack of documentation**: A known issue by the Nix community. Usually you have to wait until you're proficient enough with Nix just so you can read the source code (which is easy enough to navigate). You'll often catch yourself gathering information from scattered blogs all over the internet. 

---
**Resources**

[^1]:[Nixpkgs repository](https://github.com/NixOS/nixpkgs)

[^2]: [Packaging/Binaries](https://nixos.wiki/wiki/Packaging/Binaries)

[^3]: [Storage optimization](https://nixos.wiki/wiki/Storage_optimization)
