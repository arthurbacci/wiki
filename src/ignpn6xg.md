# Lines for a blank .vimrc

I'm not a vim enthusiast but I do like to use it for some small programs and config files. This is the configuration I use when I see a blank ``.vimrc``:

```vim
{{#include assets/snippets/lines-blank-vimrc.vim:1:15}}
```

## Persistent Undo

If I'm setting up my own machine with proper environment variables I use Vim's persistent undo. Tired of losing a lot of files due to the vice of recklessly closing terminal emulators or rebooting without double-checking I found a solution and this is Vim's persistent undo. This way I can come back to a file and know I have the history of it. 

```vim
{{#include assets/snippets/lines-blank-vimrc.vim:16:21}}
```

[.vimrc source code](assets/snippets/lines-blank-vimrc.vim)

---
**Resources**

- [david\_chisnall comment](https://lobste.rs/s/du8i6z/5_lines_i_put_blank_vimrc#c_mudy7y)