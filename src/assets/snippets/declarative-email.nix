{ pkgs, config, ... }:

{
  programs.mbsync.enable = true;
  programs.msmtp.enable = true;
  programs.notmuch = {
    enable = true;
    hooks.preNew = "mbsync --all";
  };

  services.mbsync = {
    enable = true;
    frequency = "*:0/5";
    postExec = "${pkgs.notmuch}/bin/notmuch new";
  };

  accounts.email =
    let
      path = "${config.xdg.dataHome}/mail";
    in
    {
      maildirBasePath = path;
      # look up where the certificate is located on your distro
      certificatesFile = "/etc/pki/tls/certs/ca-bundle.crt";
      accounts."name-for-your-account" =
        let
          emailUsername = "Your Name";
          emailAccount = "your@email.com";
        in
        {
          realName = emailUsername;
          address = emailAccount;
          userName = emailAccount;
          imap.host = "your.imap.host";
          smtp.host = "your.smtp.host";
          primary = true;

          maildir.path = "${emailAccount}";

          mbsync = {
            enable = true;
            create = "both";
            expunge = "both";
          };
          msmtp.enable = true;
          notmuch.enable = true;
          signature = {
            text = ''
              ---
              Your Name
            '';
            showSignature = "append";
          };
          passwordCommand = "${pkgs.pass}/bin/pass your/password";
        };
    };
}
