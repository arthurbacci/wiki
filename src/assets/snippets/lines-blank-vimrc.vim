syntax on                       " enables syntax highlight
set autoindent                  " automatically indent lines
set expandtab                   " converts tabs to spaces
set hidden                      " hides buffer instead of closing 
set hlsearch                    " highlight search results
set ignorecase                  " case-insentive search
set incsearch                   " incremental search
set noswapfile                  " we have... git nowadays
set number                      " show line numbers
set ruler                       " shows the column
set showcmd                     " shows cmds in the last line of the screen 
set tabstop=4                   " number of spaces in a tab 
set wrap                        " wrap on words and makes navigation more obvious
set wildmenu                    " tab-completion on commands
set backspace=indent,eol,start  " enable backspace
set dir=${XDG_CACHE_HOME}/vim/swap//
set backupdir=${XDG_CACHE_HOME}/vim/backup//
set undodir=${XDG_CACHE_HOME}/vim/undo//
set undofile
set undolevels=1000
set undoreload=10000