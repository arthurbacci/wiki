# Cryptocurrencies

A cryptocurrency as described in the title of the original Bitcoin white paper: [__Bitcoin: A Peer-to-Peer Electronic Cash System__](https://gateway.ipfs.io/ipfs/QmRA3NWM82ZGynMbYzAgYTSXCVM14Wx1RZ8fKP42G6gjgj). It's cash, but completely digital.

> What is needed is an electronic payment system based on cryptographic proof instead of trust, allowing any two willing parties to transact directly with each other without the need for a trusted third party. 
> 
> > **Satoshi Nakamoto**, *[Bitcoin: A Peer-to-Peer Electronic Cash System](https://gateway.ipfs.io/ipfs/QmRA3NWM82ZGynMbYzAgYTSXCVM14Wx1RZ8fKP42G6gjgj)*

## Important Properties of a Cryptocurrency

- **No third parties**: There's no need for a third party to be involved on transactions. All payments are *peer-to-peer* between *accounts* just like handing someone a dollar bill. You don't trust a institution, you trust the whole system.

- **No counterfeiting**: Counterfeiting is impossible as anyone can verify the integrity of all transactions.

- **Large and small amounts behave the same**: It makes no difference if you're sending a small or a large a amount of money. They'll have the same transaction fee (or none) and be treated as the same on the system. You can also split your money into multiple accounts without any issues.

- **Borderless**: Cryptocurrencies are global. They don't have a nationality, they don't even need to know who you are. As long as you have an internet connection (or not[^1][^2]) you're good to go.

[^1]: [Blockstream Satellite: Bitcoin blockchain broadcasts](https://blockstream.com/satellite/)

[^2]: [Build Your Own Wireless Bitcoin Service Provider — Complete With Blockstream Satellite Backhaul](https://medium.com/blockstream/build-your-own-wireless-bitcoin-service-provider-complete-with-blockstream-satellite-backhaul-165469036658)

---
**Resources**
- [Bitcoin: A Peer-to-Peer Electronic Cash System](https://gateway.ipfs.io/ipfs/QmRA3NWM82ZGynMbYzAgYTSXCVM14Wx1RZ8fKP42G6gjgj)
- [Properties of a cryptocurrency](https://whycryptocurrencies.com/properties_of_a_cryptocurrency.html)