# Logging to multiple destinations in Go

As the [log.SetOutput](https://golang.org/pkg/log/#SetOutput) function receives an [io.Writer](https://golang.org/pkg/io/#Writer) interface you can use the function [io.MultiWriter](https://golang.org/pkg/io/#MultiWriter) to specify the writers.

```go
// writing to a file and to stdout
wrt := io.MultiWriter(file, os.Stdout)
log.SetOutput(wrt)
```
