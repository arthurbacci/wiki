# Price falls benefits the holder

One of the mistakes I made (if not the biggest one) when I started investing in stocks was to wait for the best price. I'd be constantly checking prices and professional analysis to see when I'd buy the next batch of stocks. I can say that 99% of the time those analysis didn't predict the price correctly.

What made me think differently was to make a simple chart with two stocks: one that always goes up and one that fluctuates. This is exactly why a person that does [[8iyg8jq5]] should not be worried by prices. 

![Stock price](assets/images/stock_price.png)

Now compare a person buying $1000 of these stocks religiously every month and their patrimony:

![Constant Price vs Volatile Price](assets/images/constant_vs_volatile.png)