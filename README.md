# Wiki

Hi, welcome!

My name is Victor. I'm a software developer who happens to like [Go](https://golang.org) and [Nix(OS)](https://nixos.org).

This website is a place I use to store my notes and thoughts about things I'm interested. Don't be bothered if things feel incomplete or confusing. You're closer to my shower thoughts than a finished book.